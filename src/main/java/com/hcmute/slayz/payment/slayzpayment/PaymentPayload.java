package com.hcmute.slayz.payment.slayzpayment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentPayload {
  private String partnerCode;
  private String orderId;
  private String requestId;
  private Long amount;
  private String orderInfo;
  private String orderType;
  private Long transId;
  private Integer resultCode;
  private String message;
  private String payType;
  private Long responseTime;
  private String extraData;
  private String signature;
}
