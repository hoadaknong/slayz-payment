package com.hcmute.slayz.payment.slayzpayment;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionsHandler {
  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<?> handlerResourceNotFoundException(
          RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.NOT_FOUND,
                    exception.getMessage(),
                    null));
  }
}
