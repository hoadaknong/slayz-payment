package com.hcmute.slayz.payment.slayzpayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlayzPaymentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SlayzPaymentApplication.class, args);
	}

}
