FROM openjdk:17
EXPOSE 8080
ADD target/slayz-payment.jar slayz-payment.jar
ENTRYPOINT ["java","-jar","/slayz-payment.jar"]